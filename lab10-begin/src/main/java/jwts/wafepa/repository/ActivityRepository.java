package jwts.wafepa.repository;

import java.util.List;

import jwts.wafepa.model.Activity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

//	@Query("select a from Activity a where a.name = :name")
//	Activity findByName(@Param("name") String name);
	
	Page<Activity> findAll(Pageable pageable);
	
	Page<Activity> findByName(String name, Pageable pageable);
	Page<Activity> findByNameLike(String name, Pageable pageable);
}
	