var wafepaApp = angular.module('wafepaApp.controllers', []);

wafepaApp.controller('ActivitiesController', function($scope, $location, $routeParams, activityRestService) {
	
	$scope.page = 0;
	
	$scope.getActivities = function() {
		
		var parameters = {};
		if ($scope.search) {
			parameters['name'] = $scope.search;
		}
		parameters['page'] = $scope.page;
		
		activityRestService.getActivities(parameters)
			.success(function(data, status, headers) {
				$scope.activities = data;
				$scope.totalPages = headers()['total-pages'];
				$scope.successMessage = 'Everything is fine.';
			})
			.error(function() {
				$scope.errorMessage = 'Something went wrong!'
			});
	};
	
	$scope.deleteActivity = function(id, index) {
		activityRestService.deleteActivity(id)
			.success(function(data) {
				$scope.activities.splice(index, 1);
			});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams && $routeParams.id) {
			activityRestService.getActivity($routeParams.id)
				.success(function(data) {
					$scope.activity = data;
				});
		}
	};
	
	$scope.saveActivity = function() {
		activityRestService.saveActivity($scope.activity)
			.success(function(data) {
				$location.path('/activities');
			});
	};
	
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getActivities();
	};
});

wafepaApp.controller('TranslateController', function($scope, $translate) {
	
	$scope.changeLang = function(lang) {
		$translate.use(lang);
		localStorage.setItem('language', lang);
	};
});