package jwts.wafepa.web.controller;

import java.util.ArrayList;
import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;
import jwts.wafepa.web.dto.ActivityDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/activities")
public class ApiActivityController {

	@Autowired
	ActivityService activityService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ActivityDTO>> getActivities(
			@RequestParam(value="name", required=false) String name,
			@RequestParam(value="page", required=true) Integer page) {
		List<Activity> activities = null;
		Page<Activity> activitiesPage = null;
		int totalPages = 0;
		if (name == null) {
			activitiesPage = activityService.findAll(page);
		} else {
			activitiesPage = activityService.findByName(name, page);
		}
		activities = activitiesPage.getContent();
		totalPages = activitiesPage.getTotalPages();
		
		List<ActivityDTO> activitiesDTO = new ArrayList<>();
		for (Activity activity : activities) {
			activitiesDTO.add(new ActivityDTO(activity));
		}
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("total-pages", "" + totalPages);

		return new ResponseEntity<>(activitiesDTO, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ActivityDTO> getActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ActivityDTO> deleteActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity != null) {
			activityService.remove(id);
			return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ActivityDTO> saveAcivity(
			@RequestBody ActivityDTO activityDTO) {
		Activity activity = new Activity();
		activity.setName(activityDTO.getName());
		activity = activityService.save(activity);
		return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<ActivityDTO> editActivity(@PathVariable Long id,
			@RequestBody ActivityDTO activityDTO) {

		Activity activity = activityService.findOne(id);
		if (activity != null) {
			if (id != activityDTO.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			activity.setId(activityDTO.getId());
			activity.setName(activityDTO.getName());
			activity = activityService.save(activity);
			return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
