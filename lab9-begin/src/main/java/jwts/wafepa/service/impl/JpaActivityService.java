package jwts.wafepa.service.impl;

import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.repository.ActivityRepository;
import jwts.wafepa.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class JpaActivityService implements ActivityService {

	@Autowired
	private ActivityRepository activityRepository;
	
	@Override
	public Activity findOne(Long id) {
		return activityRepository.findOne(id);
	}

	@Override
	public List<Activity> findAll() {
		return activityRepository.findAll();
	}

	@Override
	public Activity save(Activity activity) {
		return activityRepository.save(activity);
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		activityRepository.delete(id);
	}

	@Override
	public List<Activity> findByName(String name) {
		return activityRepository.findByNameLike("%" + name + "%");
	}

	@Override
	public Page<Activity> findAll(int page) {
		return activityRepository.findAll(new PageRequest(page, 5));
	}

}
