package jwts.wafepa.service;

import java.util.List;

import jwts.wafepa.model.Activity;

import org.springframework.data.domain.Page;

public interface ActivityService {

	Activity findOne(Long id);
	List<Activity> findAll();
	Activity save(Activity activity);
	void remove(Long id) throws IllegalArgumentException;
	
	Page<Activity> findAll(int page);
	List<Activity> findByName(String name);
}
