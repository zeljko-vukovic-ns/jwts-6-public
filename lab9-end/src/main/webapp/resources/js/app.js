var wafepaApp = angular.module('wafepaApp', ['ngRoute',
                                             'wafepaApp.controllers',
                                             'wafepaApp.directives',
                                             'wafepaApp.services']);

wafepaApp.config([ '$routeProvider', function($routeProvider) {
	
	$routeProvider
		.when('/', { 
			templateUrl : 'resources/html/home.html'
		})
		.when('/activities', {
			templateUrl : 'resources/html/activities.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/add', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/edit/:id', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		;
}]);